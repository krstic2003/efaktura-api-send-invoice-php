<?php

$curl = curl_init();

//generated in EFAKTURE portal
$api_key = 'YOUR-API-KEY';

// requestId must be unique. One of solutions is current timestamp
$requestId = time();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://efaktura.mfin.gov.rs/api/publicApi/sales-invoice/ubl?requestId='.$requestId.'&sendToCir=No',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'<?xml version="1.0" encoding="UTF-8"?><Invoice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:cec="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:sbt="http://mfin.gov.rs/srbdt/srbdtext" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> <cbc:CustomizationID>urn:cen.eu:en16931:2017#compliant#urn:mfin.gov.rs:srbdt:2021</cbc:CustomizationID> <cbc:ID>001-test1</cbc:ID> <cbc:IssueDate>2022-12-30</cbc:IssueDate> <cbc:DueDate>2022-12-30</cbc:DueDate> <cbc:InvoiceTypeCode>380</cbc:InvoiceTypeCode> <cbc:Note>e-Faktura TEST</cbc:Note> <cbc:DocumentCurrencyCode>RSD</cbc:DocumentCurrencyCode> <cac:InvoicePeriod> <cbc:DescriptionCode>3</cbc:DescriptionCode> </cac:InvoicePeriod> <cac:ContractDocumentReference> <cbc:ID>CTR1/2021</cbc:ID> </cac:ContractDocumentReference> <cac:AccountingSupplierParty> <cac:Party> <cbc:EndpointID schemeID="9948">110480480</cbc:EndpointID> <cac:PartyName> <cbc:Name>ALEKSANDAR KRSTIĆ PR PROGRAMIRANjE ITPROGRAMIRANjE VALjEVO</cbc:Name> </cac:PartyName> <cac:PostalAddress> <cbc:CityName>VALjEVO</cbc:CityName> <cac:Country> <cbc:IdentificationCode>RS</cbc:IdentificationCode> </cac:Country> </cac:PostalAddress> <cac:PartyTaxScheme> <cbc:CompanyID>RS110480480</cbc:CompanyID> <cac:TaxScheme> <cbc:ID>VAT</cbc:ID> </cac:TaxScheme> </cac:PartyTaxScheme> <cac:PartyLegalEntity> <cbc:RegistrationName>ALEKSANDAR KRSTIĆ PR PROGRAMIRANjE ITPROGRAMIRANjE VALjEVO</cbc:RegistrationName> <cbc:CompanyID>64873792</cbc:CompanyID> </cac:PartyLegalEntity> <cac:Contact> <cbc:ElectronicMail>krstic2003@gmail.com</cbc:ElectronicMail> </cac:Contact> </cac:Party> </cac:AccountingSupplierParty> <cac:AccountingCustomerParty> <cac:Party> <cbc:EndpointID schemeID="9948">105223872</cbc:EndpointID> <cac:PartyIdentification> <cbc:ID>60871205</cbc:ID> </cac:PartyIdentification> <cac:PartyName> <cbc:Name>SAMOSTALNA ZANATSKA RADNJA VESIĆ 1</cbc:Name> </cac:PartyName> <cac:PostalAddress> <cbc:StreetName>VOJVODE MIŠIĆA 95</cbc:StreetName> <cbc:CityName>VALJEVO</cbc:CityName> <cac:Country> <cbc:IdentificationCode>RS</cbc:IdentificationCode> </cac:Country> </cac:PostalAddress> <cac:PartyTaxScheme> <cbc:CompanyID>RS105223872</cbc:CompanyID> <cac:TaxScheme> <cbc:ID>VAT</cbc:ID> </cac:TaxScheme> </cac:PartyTaxScheme> <cac:PartyLegalEntity> <cbc:RegistrationName>SAMOSTALNA ZANATSKA RADNJA VESIĆ 1</cbc:RegistrationName> <cbc:CompanyID>60871205</cbc:CompanyID> </cac:PartyLegalEntity> </cac:Party> </cac:AccountingCustomerParty> <cac:Delivery> <cbc:ActualDeliveryDate>2022-12-30</cbc:ActualDeliveryDate> </cac:Delivery> <cac:PaymentMeans> <cbc:PaymentMeansCode>30</cbc:PaymentMeansCode> <cac:PayeeFinancialAccount> <cbc:ID>155-0000000036159-76</cbc:ID> </cac:PayeeFinancialAccount> </cac:PaymentMeans> <cac:TaxTotal> <cbc:TaxAmount currencyID="RSD">30</cbc:TaxAmount> <cac:TaxSubtotal> <cbc:TaxableAmount currencyID="RSD">300</cbc:TaxableAmount> <cbc:TaxAmount currencyID="RSD">30.0</cbc:TaxAmount> <cac:TaxCategory> <cbc:ID>S</cbc:ID> <cbc:Percent>10</cbc:Percent> <cac:TaxScheme> <cbc:ID>VAT</cbc:ID> </cac:TaxScheme> </cac:TaxCategory> </cac:TaxSubtotal> </cac:TaxTotal> <cac:LegalMonetaryTotal> <cbc:LineExtensionAmount currencyID="RSD">300</cbc:LineExtensionAmount> <cbc:TaxExclusiveAmount currencyID="RSD">300</cbc:TaxExclusiveAmount> <cbc:TaxInclusiveAmount currencyID="RSD">330</cbc:TaxInclusiveAmount> <cbc:AllowanceTotalAmount currencyID="RSD">0</cbc:AllowanceTotalAmount> <cbc:PrepaidAmount currencyID="RSD">0</cbc:PrepaidAmount> <cbc:PayableAmount currencyID="RSD">330</cbc:PayableAmount> </cac:LegalMonetaryTotal> <cac:InvoiceLine> <cbc:ID>1</cbc:ID> <cbc:InvoicedQuantity unitCode="H87">1</cbc:InvoicedQuantity> <cbc:LineExtensionAmount currencyID="RSD">300</cbc:LineExtensionAmount> <cac:Item> <cbc:Name>sladoled</cbc:Name> <cac:SellersItemIdentification> <cbc:ID>010</cbc:ID> </cac:SellersItemIdentification> <cac:ClassifiedTaxCategory> <cbc:ID>S</cbc:ID> <cbc:Percent>10</cbc:Percent> <cac:TaxScheme> <cbc:ID>VAT</cbc:ID> </cac:TaxScheme> </cac:ClassifiedTaxCategory> </cac:Item> <cac:Price> <cbc:PriceAmount currencyID="RSD">300</cbc:PriceAmount> </cac:Price> </cac:InvoiceLine></Invoice>',
  CURLOPT_HTTPHEADER => array(
    'accept: text/plain',
    'ApiKey: ' . $api_key,
    'Content-Type: application/xml',
    'Cookie: SERVERID=ssp7'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;

?>