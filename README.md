# EFAKTURA API - Send Invoice PHP

## Getting started

- Generate API key in EFAKTURA portal (look in docs folder - Serbian version only)
- Replace 'YOUR-API-KEY' with generated API key in file api.php at line 6.

## Additional comments

- request-body-example.xml contains well-formed xml example of request body (UBL standard)
- other xml examples can be found here - https://www.efaktura.gov.rs/tekst/330/azurirani-primeri-xml-datoteka-po-ubl-21-standardu.php
